const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = mongoose.model('User');
const userSchema = require("../models/user.model");

module.exports.register = (req, res, next) => {
    bcrypt.hash(req.body.password, 10, function (err, hashedPw) {
        if (err) {
            res.json({
                error: err + "."
            })
            return
        }
        let user = new User({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            password: hashedPw
        });
        user.save()
            .then(user => {
                res.json({
                    message: 'User created!'
                })
            })
            .catch(error => {
                res.json({
                    message: 'An error occured:' + error
                })
                return
            })
    })
}

module.exports.login = (req, res, next) => {
    let getUser;
    userSchema.findOne({
        email: req.body.email
    }).then(user => {
        if (!user) {
            return res.status(401).json({
                message: "Authentication failed"
            });
        }
        getUser = user;
        return bcrypt.compare(req.body.password, user.get('password'));
    }).then(response => {
        if (!response) {
            return res.status(401).json({
                message: "Authentication failed"
            });
        }
        let jwtToken = jwt.sign({
            email: getUser.email,
            userId: getUser._id
        }, "longer-secret-is-better", {
            expiresIn: "1h"
        });
        res.status(200).json({
            token: jwtToken,
            expiresIn: 3600,
            _id: getUser._id
        });
    }).catch(err => {
        return res.status(401).json({
            message: "Authentication failed"
        });
    });
};